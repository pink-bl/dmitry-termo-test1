

file_path = '/home/dmitry/IOCs/test/Table.txt'

# Initialize an empty list to store the data
data = []

# Open and read the file
with open(file_path, 'r') as file:
    for line in file:
        # Split each line into a list of values using whitespace as the delimiter
        values = line.split()
        # Convert the values to floats and append them to the data list
        values = [float(val) for val in values]
        data.append(values)

